# Dependencies
import sublime, sublime_plugin, re, string
from datetime import datetime


#
# The plugin searches for 2create CSS Standards comments (both main and sub).
# Then list them in the quick panel for faster use.
#
class GotoCssCommentCommand( sublime_plugin.TextCommand ) :

	# All sections will be stored here
	sections = {}


	# Go go go
	def run(self, edit):

		# Enable only for css scopes
		is_css = self.view.match_selector( self.view.sel()[0].b, 'source.css' )

		if not is_css :
			return

		# Get the CSS sections
		self.sections = self.prepare_sections_form_menu( self.get_all_sections() )

		# Show the quick panel
		self.view.window().show_quick_panel( self.sections[ 'names' ], self.on_done )


	# Get all sections (main & sub)
	def get_all_sections( self ) :

		# Get main sections
		main_regex_start = "\/\*\s-*\s\*\\\\\n"
		main_regex_end = "\n\\\\\*\s-*\s\*\/"
		main_sections = self.get_sections( main_regex_start, main_regex_end, "", 2 )

		# Get sub sections
		sub_regex_start = "\/\*\s{2}"
		sub_regex_end = "\s{2}\*\/"
		sub_sections = self.get_sections( sub_regex_start, sub_regex_end, '-- ', 1 )

		# Combine main and sub sections
		all_sections = {}
		all_sections = dict(main_sections.items()).copy()
		all_sections.update( dict(sub_sections.items()))

		# Sort the by line number
		sorted_keys = sorted( all_sections.keys() )
		
		all_names = []
		all_keys = []

		for i in sorted_keys :
			all_names.append( all_sections[i] )
			all_keys.append( i )

		return { "names": all_names, "lines": all_keys }


	def prepare_sections_form_menu( self, sections ):
		num = len( sections[ "names" ] )

		for i in range(0,num):
			line_num = sections[ "lines" ][ i ]
			sections[ "names" ][ i ] = sections[ "names" ][ i ] + ' (line ' + str(line_num) + ')'
		return sections


	# Get specific section via provided regex
	def get_sections( self, regex_start, regex_end, name_prefix, line_offset ):

		# Create the regex
		regex = regex_start + "(.*?)" + regex_end

		# Match
		matches = self.view.find_all( regex )

		sections = {}

		# Loooping ...
		for match in matches :
			line = self.view.line( match )
			
			# Clean the section name
			section_name = self.view.substr( line )
			section_name = re.sub( regex_start, "", section_name )
			section_name = re.sub( regex_end, "", section_name )

			# Add the prefix
			section_name = name_prefix + section_name.strip()

			# Line & column
			section_line, section_column = self.view.rowcol(line.begin())

			# Add line offset
			section_line = section_line + line_offset

			# Add section name suffix - line number
			# section_name = section_name + ' (line ' + str(section_line) + ')'

			sections[ section_line ] = section_name

		return sections


	# On Done/Select event
	def on_done(self, index):

		# Check if it was canceled
		if index == -1 :
			return

		# Get the line of the selected sections
		selected_line = self.sections[ 'lines' ][ index ]

		self.view.run_command( "goto_line", { "line": selected_line } )

		return


#
# The plugin creates nice Table of Contents for a CSS file
# which uses 2create CSS Standards comments
#

class CssTableOfContentsCommand( GotoCssCommentCommand ) :

	# Go go go
	def run(self, edit):

		# Enable only for css scopes
		is_css = self.view.match_selector( self.view.sel()[0].b, 'source.css' )

		if not is_css :
			return


		# Remove the previous contents
		matches = self.view.find_all( "\/\*{2}((\r\n?|\n|.)*?)\*{2}\/(\r\n?|\n)*" )

		for match in matches :
			self.view.erase( edit, match )


		# Get the CSS sections
		self.sections = self.get_all_sections()
		section_names = self.sections[ "names" ]
		section_lines = self.sections[ "lines" ]

		num_sections = len( section_names )
		num_extra_lines = 5

		current_time = str( datetime.now().strftime( "%Y-%m-%d %H:%M:%S" ) )


		# Start outputing
		output = "/**";
		output += "\n\t";
		output += "Table of Contents";
		output += "\n";
		output += "\t" + "updated on " + current_time;
		output += "\n\n";

		total_dashes = 50


		j = 0;
		for i in range(0, num_sections): 
			section_name = section_names[ i ]
			section_line = section_lines[ i ]

			# check if this is main or sub section
			if section_name.find( '--' ) == -1 :
				j += 1
				section_name = str( "%02d" % (j,) ) + '. ' + section_name
			else :
				section_name = '   ' + section_name.replace("--", "")

			# dashes
			name_len = len( section_name )
			num_dashes = total_dashes - name_len
			dashes = ''.join(['-' for s in range(num_dashes)])

			output += "\t"
			output += section_name
			output += ' ' + dashes + ' '
			output += '(line ' + str( section_line + num_sections + num_extra_lines ) + ')'
			output += "\n"

		output += "";
		output += "**/";
		output += "\n\n";


		# Insert the outpot
		line = self.view.line(0)
		self.view.insert(edit, line.begin(), output)

		# Goto to first line
		self.view.run_command( "goto_line", { "line": 1 } )

		return 